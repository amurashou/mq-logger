package by.iba.api.rp.enums;

/**
 * This enum covers type of responses
 *
 * @author Andrei Ruban - software engineer IBA IT Park Minsk.
 * @version 30.06.2015
 */

public enum ResponseTypes {
    SUCCESS_RESPONSE, ERROR_RESPONSE, MESSAGE_NOT_PROVIDED, DELIVERED_DEFAULT_QUEUE
}
