package by.iba.api.rp;

import by.iba.api.rp.enums.ResponseTypes;

/**
 * This class is an implementation of success response with ResponseType SUCCESS_RESPONSE
 *
 * @author Andrei Ruban - software engineer IBA IT Park Minsk.
 * @version 30.06.2015
 */

public class SuccessResponse extends BaseResponse{
    private ResponseTypes messageType = ResponseTypes.SUCCESS_RESPONSE;

    public ResponseTypes getMessageType(){
        return messageType;
    }
}
